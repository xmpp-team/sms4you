Source: sms4you
Section: comm
Priority: optional
Maintainer: Debian XMPP Maintainers <pkg-xmpp-devel@lists.alioth.debian.org>
Uploaders: Martin <debacle@debian.org>
Build-Depends: debhelper-compat (= 13),
	dh-python,
	hovercraft,
	python3,
	python3-asgiref,
	python3-dbussy,
	python3-docutils,
	python3-sdnotify,
	python3-setuptools,
	python3-slixmpp,
	python3-systemd,
Standards-Version: 4.1.3
Rules-Requires-Root: no
Homepage: https://sms4you-team.pages.debian.net/sms4you
Vcs-Browser: https://salsa.debian.org/xmpp-team/sms4you
Vcs-Git: https://salsa.debian.org/xmpp-team/sms4you.git

Package: sms4you
Section: comm
Architecture: all
Depends: adduser,
	modemmanager,
	python3,
	python3-dbussy,
	python3-sdnotify,
	python3-systemd,
	sms4you-xmpp (= ${source:Version}) | sms4you-email (= ${source:Version}),
	${misc:Depends},
	${python3:Depends},
Recommends: sms4you-doc
Suggests: usb-modeswitch
Description: Personal gateway connecting SMS to XMPP or email
 sms4you forwards messages from and to SMS and connects either with
 sms4you-xmpp or sms4you-email to choose the other mean of
 communication. Nice for receiving or sending SMS, independently from
 carrying a SIM card.
 .
 This package contains the common files.

Package: sms4you-xmpp
Section: comm
Architecture: all
Depends: prosody | ejabberd | xmpp-server,
	python3,
	python3-slixmpp,
	sms4you (= ${source:Version}),
	${misc:Depends},
	${python3:Depends},
Description: Personal gateway connecting SMS to XMPP
 sms4you forwards messages from and to SMS and connects either with
 sms4you-xmpp or sms4you-email to choose the other mean of
 communication. Nice for receiving or sending SMS, independently from
 carrying a SIM card.
 .
 This package contains the XMPP component.

Package: sms4you-email
Section: comm
Architecture: all
Depends: python3,
	python3-asgiref,
	sms4you (= ${source:Version}),
	${misc:Depends},
	${python3:Depends},
Description: Personal gateway connecting SMS to email
 sms4you forwards messages from and to SMS and connects either with
 sms4you-xmpp or sms4you-email to choose the other means of
 communication. Nice for receiving or sending SMS, independently from
 carrying a SIM card.
 .
 This package contains the email forwarding daemon.

Package: sms4you-doc
Section: doc
Architecture: all
Depends: hovercraft,
	${misc:Depends},
Suggests: libjs-impress,
Description: Personal gateway connecting SMS to XMPP or email - documentation
 sms4you forwards messages from and to SMS and connects either with
 sms4you-xmpp or sms4you-email to choose the other means of
 communication. Nice for receiving or sending SMS, independently from
 carrying a SIM card.
 .
 This package contains the documentation.
