SOURCES=sms4you sms4you-email sms4you-xmpp
LENGTH=96

all: sms4you.1 index.html

check: $(SOURCES)
	flake8 --max-line-length=$(LENGTH) $^
	black --check --line-length $(LENGTH) $^
	#pylint -E $^

black: $(SOURCES)
	black --line-length $(LENGTH) $^


sms4you.1: README.rst
	cat $< \
	    | sed -e '0,/^$$/s//:Manual section: 1\n/' \
	    | rst2man \
	    | grep -v '^\[image: ' \
	      > $@

index.html: README.rst
	rst2html $< > $@

clean:
	rm -f sms4you.1 index.html
