sms4you-xmpp - XMPP component for SMS
=====================================

This is a gateway between SMS and XMPP. It runs as an XMPP server 
component, i.e. you need your own XMPP server.

If you are interested in an XMPP-SMS gateway, but do not like to
maintain your own XMPP server, please take a look at
`JMP <https://jmp.chat/>`__.

Requirements
------------

-  An XMPP server must be running on the system, e.g.
   `Ejabberd <https://www.ejabberd.im/>`__ or
   `Prosody <https://www.prosody.im/>`__, which at least has s2s
   connection to the outside world
-  ModemManager must be running on the system, it will be accessed via
   DBus and your modem must, of course, be supported by ModemManager
-  `Python 3 <https://www.python.org>`__
-  `python3-dbussy <https://github.com/ldo/dbussy>`__
-  `python3-sdnotify <https://github.com/bb4242/sdnotify>`__
-  `python3-slixmpp <https://lab.louiz.org/poezio/slixmpp>`__

Installation
------------

-  ``sudo cp sms4you-xmpp/sms4you-xmpp.py /usr/local/sbin/sms4you-xmpp``
-  ``sudo cp sms4you-xmpp/sms4you-xmpp.service /etc/systemd/system/``
-  ``sudo mkdir /etc/sms4you/``

Configuration
-------------

Ejabberd
~~~~~~~~

**TODO**

Prosody
~~~~~~~

Add the following lines to your prosody configuration, e.g.
``/etc/prosody/prosody.cfg.lua``:

::

    Component "sms4you.localhost"
        component_secret = "Mb2.r5oHf-0t"

sms4you-xmpp
~~~~~~~~~~~~

Save the password (``Mb2.r5oHf-0t``) in
``/etc/sms4you/xmpp_component_password.txt``. and configure the XMPP
account (JID), which should be allowed to send and receive SMS in
``/etc/sms4you/sms4you.ini``:

::

    USER_JID=user@jabberserver.foo.bar

You probably need to configure more details in
``/etc/systemd/system/sms4you-xmpp.service``. Look at the command line
arguments: ``/usr/local/sbin/sms4you-xmpp --help``.

Let's Go
--------

-  ``sudo systemctl enable sms4you-xmpp.service``
-  ``sudo systemctl start sms4you-xmpp.service``

Future
------

Some improvement ideas: \* support multiple modems, each with its own
associated users \* optionally send SMS to MUC, MIX or PubSub \*
restrictions for sending SMS, e.g. no international SMS or limits per
month
